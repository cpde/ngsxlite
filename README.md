# NGSolve(+AddOns)-enriched JupyterLite Demo Repository

We use the ngsx-enriched pyodide-instance generated from https://gitlab.gwdg.de/lehrenfeld/ngsx-pyodide to run ngsolve in the browser. The used file can be downloaded from

https://lehrenfeld.pages.gwdg.de/ngsx-pyodide/ngsx_pyodide.tar.bz2

... more to be added ...
